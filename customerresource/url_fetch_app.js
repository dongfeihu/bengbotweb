var fetch = require('node-fetch');

var UrlFetchApp = {};

UrlFetchApp.fetch = function(url, option) {
  var fetchOption = {};

  fetchOption.method = option.method;
  fetchOption.body = option.payload;
  fetchOption.headers = option.headers;

  if (option.contentType) {
    if (!fetchOption.headers) {
      fetchOption.headers = {};
    }
    fetchOption.headers["Content-Type"] = option.contentType;

  }
  return fetch(url, fetchOption);
};
UrlFetchApp.get = function(url) {
  return fetch(url);
};



module.exports = UrlFetchApp;
