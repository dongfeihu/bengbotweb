var UrlFetchApp = require('./url_fetch_app');
async function PostTelegram(payload) {
  var data = {
    'contentType': 'application/json',
    "method": "post",
    "payload": JSON.stringify(payload)
  };
  try {
    var token = '1031625550:AAEqzMzCrmcItK-w4mxvNzm_X1DdPWhEpbs';
    var response = await UrlFetchApp.fetch("https://api.telegram.org/bot"+token+"/", data);

    var json = await response.json();
    return Promise.resolve(json);
  } catch (e) {
    var errorLog = {
      error: e,
      payload: payload
    };
    return {error: errorLog};
  }
}
module.exports = PostTelegram;