var Utilities = {};
Utilities.base64Encode = function (text) {
    return Buffer.from(text).toString('base64');
};

module.exports = Utilities;