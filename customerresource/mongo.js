var UrlFetchApp = require('./url_fetch_app');
var Utilities = require('./utilities');


var baseUrl = "https://api.mlab.com/api/1/";
var dbUrl = baseUrl + "databases/mongo/";
var collectionUrl = dbUrl + "collections/";

var channelColl = "channels";
var channelIdsColl = "channel-ids";
var telegramChatIdsColl = "telegram-chat-ids";
var telegramChatsColl = "telegram-chats";
var updateTimeColl = "update-time";
var telegramChatUpdateTimeColl = "telegram-chat-update-time";
var drawColl = "draws";
var contextColl = "context";
var videoColl = "videos";
var logColl = "logs";
var videoDetailsColl = "video-details";
var allVideosColl = "all-videos";
groupSettingsColl = "group-settings";
var toTelegram = "to-telegram";
var mongo = {};
// 改成你自己的restheart 网址
//var restheartUrl = 'http://restheart:8080/bengbot/';
var restheartUrl = 'https://testusa.sauween.com/bengbot/'
mongo.insert = function(collection, data) {
  var db = restheartUrl + collection;
  var option = getInsertOption(data);
  var response = UrlFetchApp.fetch(db, option);
  return response
};

function testRestHeart(){
  var data = {"$addToSet":{"array" : "bar2"}};
  mongo.setOne("testColl/5c75be5e294610d3cbaa39f9", data);
}

mongo.replace = function(collection, query, data) {
  var db = restheartUrl + collection;
  if (query){
    db += "?" + query;
  }
  var option = getPutOption(data);
  var url = encodeURI(db);
  return UrlFetchApp.fetch(url, option);
};

mongo.setOne = function(urlWithId, data) {
  var db = restheartUrl + urlWithId;
  var option = getPatchOption(data);
  option.muteHttpExceptions = true;
  return UrlFetchApp.fetch(encodeURI(db), option);
};

mongo.setMany = function(collection, query, data) {
  var db = restheartUrl + collection;
  if (query){
    db += "/*?" + query;
  }
  var option = getPatchOption(data);
  var url = encodeURI(db);
  return UrlFetchApp.fetch(url, option);
};

mongo.set = mongo.setMany;
mongo.remove = function(collection, query) {
  var db = restheartUrl + collection;
  if (query){
    db += "/*?" + query;
  }
  var option = getDeleteOption();
  var url = encodeURI(db);
  return UrlFetchApp.fetch(url, option);
};


mongo.get = function(collection, query) {
  var db = restheartUrl + collection;

  if (query){
    db += "?" + query;
  }

  var option = getGetOption();
  return UrlFetchApp.fetch(encodeURI(db), option).then(x => x.json()).then(json => json);
};


function getInsertOption(data){
  var option = {
    "method": "post",
    'contentType': 'application/json',
    'headers': {"Authorization": "Basic " + Utilities.base64Encode('sauween' + ":" + 'MSUtqUTmb0xo76M49k5LCvYnai3BThCn')},
    "muteHttpExceptions": true, 
    "payload": JSON.stringify(data)
  }
  return option;
}
function getDeleteOption(){
  var option = {
    "method": "delete",
    'contentType': 'application/json',
    'headers': {"Authorization": "Basic " + Utilities.base64Encode('sauween' + ":" + 'MSUtqUTmb0xo76M49k5LCvYnai3BThCn')},
  }
  return option;
}
function getPutOption(data){
  var option = {
    "method": "put",
    'contentType': 'application/json',
    'headers': {"Authorization": "Basic " + Utilities.base64Encode('sauween' + ":" + 'MSUtqUTmb0xo76M49k5LCvYnai3BThCn')},
    "payload": JSON.stringify(data)
  }
  return option;
}
function getPatchOption(data){
  var option = {
    "method": "patch",
    'contentType': 'application/json',
    'headers': {"Authorization": "Basic " + Utilities.base64Encode('sauween' + ":" + 'MSUtqUTmb0xo76M49k5LCvYnai3BThCn')},
    "payload": JSON.stringify(data)
  }
  return option;
}
function getGetOption(){
  var option = {
    "method": "get",
    'headers': {"Authorization": "Basic " + Utilities.base64Encode('sauween' + ":" + 'MSUtqUTmb0xo76M49k5LCvYnai3BThCn')},
  }
  return option;
}

module.exports = mongo;