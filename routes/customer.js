var express = require('express');
var router = express.Router();
var mongo = require('../customerresource/mongo');
/* GET home page. */
router.get('/', async function (req, res, next) {
    if (req.query && req.query.findphone) {
        var query = {
            photo: req.query.findphone * 1
        }
        query = 'filter=' + JSON.stringify(query)
        data = await mongo.get('userls', query).then(x => {
            x = x._embedded
            var data = {}
            data.payload = x
            data.action = 'findphone'
            data.findphone = req.query.findphone
            return data
        })
    } else {
        data = await mongo.get('userls').then(x => {
            x = x._embedded
            var data = {}
            data.payload = x
            data.action = 'ls'
            data.next = 1
            return data
        })
    }
    // console.log(JSON.stringify(data))
    res.render('index', { title: '猪哥查号群后台管理', data: data });
});
router.post('/', async function (req, res, next) {
    console.log(req.body)
    body = req.body
    if (body.next) {  //翻页
        query = 'page=' + body.next
        data = await mongo.get('userls', query).then(x => {
            x = x._embedded
            var data = {}
            data.payload = x
            data.action = 'ls'
            data.next = body.next
            return data
        })
        res.render('index', { title: '猪哥查号群后台管理', data: data });
        return;
    }
    if (body.action) {   //修改或者删除
        switch(body.action){
            default:
            case 'edit':
                var query = {
                    photo: body.phone * 1
                }
                query = 'filter=' + JSON.stringify(query)
                data = await mongo.get('userls', query).then(x => {
                    return x._embedded
                })
                // console.log(data[0]);
                res.render('edit', { title: '猪哥查号群后台管理 - 编辑', data: data[0] });
                break;
            case 'remove':
             //删除这个数据
                var query = {
                    photo: body.phone 
                }
                query = 'filter=' + JSON.stringify(query)
                mongo.remove('userls', query)
                data = {remove:true}
                break;
            case 'doedit':
                var query = {
                    photo: body.phone 
                }
                console.log(body.action);
                console.log(body.image);
                console.log(body.oldnote);
                console.log(body.note);

                // query = 'filter=' + JSON.stringify(query)
                // datas = {$set:{}}
                // if (body.payload.data.oldnote){  //如果修改历史评论请传递 data.oldnote
                //     datas.$set.oldnote = body.payload.data.oldnote
                // }
                // if (body.payload.data.note){   //如果修改历最新评论请传递 data.note
                //     datas.$set.note = body.payload.data.note
                // }
                // if (body.payload.data.image){  //如果修改照片请传递 data.image   image是一个网址
                //     datas.$set.image = body.payload.data.image
                // }
                // mongo.setMany('userls', query,datas)
                return true;
                break;
        }
    }
});
module.exports = router;
